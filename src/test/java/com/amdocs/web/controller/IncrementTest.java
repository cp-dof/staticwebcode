package com.amdocs.web.controller;
import com.amdocs.Increment;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
    @Test
    public void testAdd() throws Exception {

    Increment inc = new Increment();
        int k= inc.getCounter();
        assertEquals("Add", 1, k);
        k = inc.getCounter();
        assertEquals("Add", 2, k);

    }
   
    @Test
    public void testSub0() throws Exception {

        int k= new Increment().decreasecounter(0);
        assertEquals("Sub", 1, k);

    }

    @Test
    public void testSub1() throws Exception {

        int k= new Increment().decreasecounter(1);
        assertEquals("Sub", 1, k);

    }

    @Test
    public void testSub2() throws Exception {

        int k= new Increment().decreasecounter(100);
        assertEquals("Sub", 1, k);

    }
}
